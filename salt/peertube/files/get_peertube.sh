#!/bin/bash
cd /var/www/peertube/versions
VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4) && \
    echo "Latest Peertube version is $VERSION"
wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip"
unzip -q peertube-${VERSION}.zip && rm peertube-${VERSION}.zip

cd /var/www/peertube/
ln -s versions/peertube-${VERSION} ./peertube-latest
cd ./peertube-latest && yarn install --production --pure-lockfile
