#!/bin/bash
cp /var/www/peertube/peertube-latest/support/systemd/peertube.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable peertube
systemctl start peertube
cat <<EOF
######################################################################################################################################
######################################################################################################################################
The administrator password is automatically generated and can be found in the logs. You can set another password with:
cd /var/www/peertube/peertube-latest && NODE_CONFIG_DIR=/var/www/peertube/config NODE_ENV=production npm run reset-password -- -u root
######################################################################################################################################
######################################################################################################################################
EOF
